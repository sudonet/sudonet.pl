---
title: "Random Files"
date: 2025-02-21T10:32:11+01:00
draft: false
author: Patryk Kuś

summary: How to create "truly" random and fast files for testing IO performance.

tags: 
  - linux

categories:
  - tips

toc: 
  enable: false
  auto: true
---

# How to create "truly" random and fast files for testing IO performance

```
#!/bin/sh

for var in {1..1000}
do
    dd if=<(openssl enc -aes-256-ctr \
        -pass pass:"$(dd if=/dev/urandom bs=128 count=1 2>/dev/null | base64)" \
        -nosalt < /dev/zero) \
        bs=1M \
        count=100 \
        iflag=fullblock \
        of=`echo $RANDOM | md5sum | head -c 20; echo;`
done
```
